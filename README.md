# C++ libcinder obj mesh movement and peripheral inputs



## Description
Simple minimal demo of obj mesh loading and translation using libcinder. All assets used can be found in the `C++ obj mesh loading` repo.

## License
(C) Mehdi Msayib
