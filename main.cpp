#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/ObjLoader.h"

#include <iostream>

using namespace ci;
using namespace ci::app;
using namespace gl;


class Test: public App {
public:
  void keyDown(KeyEvent key_event) override;
  void keyUp(KeyEvent key_event) override;
  void setup() override;
  void update() override;
  void draw() override;

  BatchRef cube;
  mat4 cube_matrix;
  TextureRef tex;
  CameraPersp camera;
  vec3 pos;
  float rot;

};

void app_settings(Test::Settings* settings){
}

void Test::setup(){
  camera.setPerspective(50.0, getWindowAspectRatio(),0.05,500.0);
  camera.lookAt(vec3(3.0,0.0,0.0), vec3(0.0,0.0,0.0));

  ObjLoader load_mesh(loadFile("cube.obj"));
  cube=Batch::create(load_mesh, getStockShader(ShaderDef().texture()));
  tex=Texture::create(loadImage("grid.png"));
  gl::enableDepthRead(true);
  gl::enableFaceCulling(true);

}


void Test::keyDown(KeyEvent key_event){
  if (key_event.getCode()==KeyEvent::KEY_w){
    pos.x=0.05;
  }
  if (key_event.getCode()==KeyEvent::KEY_s){
    pos.x=-0.05;
  }
  if (key_event.getCode()==KeyEvent::KEY_a){
    pos.z=0.05;
  }
  if (key_event.getCode()==KeyEvent::KEY_d){
    pos.z=-0.05;
  }
}
void Test::keyUp(KeyEvent key_event){
  if (key_event.getCode()==KeyEvent::KEY_w || key_event.getCode()==KeyEvent::KEY_s){
    pos.x=0.0;
  }
  if (key_event.getCode()==KeyEvent::KEY_a || key_event.getCode()==KeyEvent::KEY_d){
    pos.z=0.0;
  }
  if (key_event.getCode()==0){
    pos.x=0.0;
    pos.z=0.0;
  }
}


void Test::update(){
}

void Test::draw(){
  clear(ci::Color::black(), true);
  setMatrices(camera);

  {

    cube_matrix*=glm::translate(pos);
    cube_matrix*=glm::rotate(0.001f,vec3(0.0,1.0,0.0));
    multModelMatrix(cube_matrix);
    (*cube).draw();
    (*tex).bind();
  }
}



CINDER_APP(Test, RendererGl, app_settings)
